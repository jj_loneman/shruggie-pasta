# Shruggie Pasta

> Created by JJ Loneman

Need to create a hotkey to paste a shruggie ¯\\_(ツ)_/¯ emoticon? You've come to the right place!

1. Create a directory to hold a couple of scripts that you will created. I just created mine in `C:\scripts`.

1. Use Notepad++ to create a PowerShell script titled `Insert-Shruggie.ps1` with the following code:

        # Insert shruggie to the clipboard
        Set-Clipboard -Value "¯\_(ツ)_/¯"

        # Load up the Forms module to use for the below commands
        [void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')

        # Send Alt+Tab to change focus back to the window you were on
        [System.Windows.Forms.SendKeys]::SendWait("%{TAB}")

        # Simulate a Ctrl+V keypress to paste shruggie from clipboard
        [System.Windows.Forms.SendKeys]::SendWait("^{v}")

1. Next, change the encoding of the file in Notepad++ by going to **Encoding > Encode in UTF-8-BOM**. **Make sure to save the file.**

    ![Step 0 - Encoding the PowerShell file in Notepad++](images/step-0-encode-ps-file.png)

    **Note**: Windows PowerShell, in the absence of BOM, interprets source as "ANSI"-encoded, referring to the legacy, single-byte, extended-ASCII code page in effect, such as Windows-1252 on US-English system, and would therefore interpret UTF-8-encoded source code incorrectly. This is why you must change the file encoding in order for the script to work.

2. In the same directory, create a new file called `hide-ps-console.vbs` with the following code (the file doesn't need any special encodings):

        CreateObject("WScript.Shell").Run "" & WScript.Arguments(0) & "", 0, False

3. Right-click on an empty space on your desktop and click **New > Shortcut**.

    ![Step 1 - Creating a new shortcut](images/step-1-shortcut.png)

4. Copy the following text and paste it into a text editor.

        C:\Windows\System32\wscript.exe C:\{your-scripts-location}\hide-ps-console.vbs "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -executionpolicy bypass -windowstyle hidden -noninteractive -file C:\{your-scripts-location}\Insert-Shruggie.ps1"

    Replace `{your-scripts-location}` with the location of where you cloned the files from this repo. Copy the text to the clipboard.

5. Paste the command into the `location` box of the wizard. Click **Next**.

    ![Step 2 - Defining the shortcut location](images/step-2-location.png)

6. Give a name for the shortcut and click **Finish**.

    ![Step 3 - Naming the shortcut](images/step-3-shortcut-name.png)

7. Next, right-click the shortcut and select **Properties**.

    ![Step 4 - Editing the shortcut properties](images/step-4-shortcut-properties.png)

8. Click in the **Shortcut key** input field and literally press the key combination you want to assign to it. I did `Ctrl + Shift + Alt + S` for mine so as to not potentially conflict with other application keybindings. Then press **OK**.

    ![Step 5 - Assigning the shortcut key](images/step-5-shortcut-key.png)

9. Now, in any editable area, press your hotkey, wait a second or so, and voila: shruggie!

    ![Step 6 - Profit](images/step-6-profit.png)