﻿# Insert shruggie to the clipboard
Set-Clipboard -Value "¯\_(ツ)_/¯"

# Load up the Forms module to use for the below commands
[void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')

# Send Alt+Tab to change focus back to the window you were on
[System.Windows.Forms.SendKeys]::SendWait("%{TAB}")

# Simulate a Ctrl+V keypress to paste shruggie from clipboard
[System.Windows.Forms.SendKeys]::SendWait("^{v}")